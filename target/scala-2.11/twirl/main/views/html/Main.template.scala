
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object Main extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Html,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String)(content: Html):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.32*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html>
  <head>
    <title>Gen2Sat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

      <!--  Load site-specific customizations after bootstrap. -->
    <link rel="stylesheet" media="screen" href=""""),_display_(/*11.50*/routes/*11.56*/.Assets.at("stylesheets/main.css")),format.raw/*11.90*/("""">
    <link rel="shortcut icon" type="image/png" href=""""),_display_(/*12.55*/routes/*12.61*/.Assets.at("images/favicon.png")),format.raw/*12.93*/("""">

      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
          <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
          <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
        <![endif]-->
  </head>
  <body>
  
    <header class="navbar navbar-inverse bs-docs-nav" id="top" role="banner">
  <div class="container" id="headerDiv">
    <div class="navbar-header">
      <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="?id=blank" class="navbar-brand">
      	<b>Gen2Sat</b>: Sat-based Decision Procedure for Analytic Pure Sequent Calculi
   	</a>
    </div>
<!-- 
    <nav class="collapse navbar-collapse bs-navbar-collapse">
      <ul class="nav navbar-nav">
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="?id=about" ><font color="white">ABOUT</font></a></li>
      </ul>
    </nav>
 -->
  </div>
</header>
  
  
    """),_display_(/*48.6*/content),format.raw/*48.13*/("""
    """),format.raw/*49.5*/("""<script src="http://code.jquery.com/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <!-- Enable tooltips. Used primarily to validate that JQuery + Bootstrap JS are loaded. Remove this script if you don't want tooltips. -->
    <script type="text/javascript">
    jQuery(function ($) """),format.raw/*54.25*/("""{"""),format.raw/*54.26*/("""
        """),format.raw/*55.9*/("""$("[rel='tooltip']").tooltip()
    """),format.raw/*56.5*/("""}"""),format.raw/*56.6*/(""");
    </script>
  </body>
</html>
"""))}
  }

  def render(title:String,content:Html): play.twirl.api.HtmlFormat.Appendable = apply(title)(content)

  def f:((String) => (Html) => play.twirl.api.HtmlFormat.Appendable) = (title) => (content) => apply(title)(content)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/Main.scala.html
                  HASH: 6117e35dba79131a437c0433c9553994d3e18233
                  MATRIX: 727->1|845->31|873->33|1255->388|1270->394|1325->428|1409->485|1424->491|1477->523|2769->1789|2797->1796|2829->1801|3217->2161|3246->2162|3282->2171|3344->2206|3372->2207
                  LINES: 26->1|29->1|31->3|39->11|39->11|39->11|40->12|40->12|40->12|76->48|76->48|77->49|82->54|82->54|83->55|84->56|84->56
                  -- GENERATED --
              */
          