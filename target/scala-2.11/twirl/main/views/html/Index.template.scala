
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object Index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[Form[views.formdata.QueryFormData],String,String,List[String],String,List[String],String,Map[String, Boolean],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(queryForm: Form[views.formdata.QueryFormData], 
result: String,
bottomLine: String, predefinedSystems: List[String],
chosenPredefinedSystem: String, 
paralyzerOptions: List[String],
rulesHeight: String,
latexCheckBoxMap:Map[String, Boolean] ):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import helper._

Seq[Any](format.raw/*7.40*/(""" 
"""),format.raw/*9.1*/("""

"""),format.raw/*11.1*/("""<script type="text/javascript"
	src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

<script type="text/x-mathjax-config">
MathJax.Hub.Config("""),format.raw/*16.20*/("""{"""),format.raw/*16.21*/("""
  """),format.raw/*17.3*/("""tex2jax: """),format.raw/*17.12*/("""{"""),format.raw/*17.13*/("""inlineMath: [['$','$'], ['\\(','\\)']]"""),format.raw/*17.51*/("""}"""),format.raw/*17.52*/("""
"""),format.raw/*18.1*/("""}"""),format.raw/*18.2*/(""");
</script>

	"""),_display_(/*21.3*/Main("Index")/*21.16*/ {_display_(Seq[Any](format.raw/*21.18*/("""
		"""),format.raw/*22.3*/("""<div id="containerDiv" class="container">
			<div id="formDiv" class="well">
		
				"""),_display_(/*25.6*/form(routes.Application.postIndex(), 'class -> "form-horizontal")/*25.71*/ {_display_(Seq[Any](format.raw/*25.73*/("""
					"""),_display_(/*26.7*/fieldset(queryForm, predefinedSystems, chosenPredefinedSystem,
					paralyzerOptions, rulesHeight, latexCheckBoxMap)),format.raw/*27.54*/("""
				""")))}),format.raw/*28.6*/("""
			"""),format.raw/*29.4*/("""</div>
		
			"""),_display_(/*31.5*/if(flash.containsKey("success"))/*31.37*/ {_display_(Seq[Any](format.raw/*31.39*/("""
				"""),format.raw/*32.5*/("""<div class="well" id="bottomLineDiv">
					"""),_display_(/*33.7*/{scala.xml.Unparsed(bottomLine)}),format.raw/*33.39*/("""</div>
			
			
				<div class="well" id="resultDiv" white-space="pre-line">
					<div id="success-message" class="text-success">
						"""),_display_(/*38.8*/{scala.xml.Unparsed(result.replace("\\\\","\\"))}),format.raw/*38.57*/("""
						"""),_display_(/*39.8*/flash/*39.13*/.get("success")),format.raw/*39.28*/("""</div>
				</div>
			""")))}),format.raw/*41.5*/(""" 
			"""),_display_(/*42.5*/if(flash.containsKey("error"))/*42.35*/ {_display_(Seq[Any](format.raw/*42.37*/("""
				"""),format.raw/*43.5*/("""<div class="well">
					<div id="error-message" class="text-danger">"""),_display_(/*44.51*/flash/*44.56*/.get("error")),format.raw/*44.69*/("""
					"""),format.raw/*45.6*/("""</div>
				</div>
			""")))}),format.raw/*47.5*/("""
		
						"""),format.raw/*49.7*/("""<div id="aboutDiv" class="well">
					<p class="big">
						Gen2Sat is a tool for deciding derivability in
						 analytic propositional pure sequent calculi, 
						by reducing it to classical satisfability. The reduction is based on the paper:						<i>
						<a href="http://link.springer.com/chapter/10.1007%2F978-3-319-08587-6_6" target="_blank">
						SAT-based Decision Procedure for Analytic Pure Sequent Calculi</a>, 
						<a href="http://www.mpi-sws.org/~orilahav/" target="_blank">
						Ori Lahav</a>, 
						<a href="http://cs.tau.ac.il/research/yoni.zohar/">
						Yoni Zohar</a>.
						Proceedings of 
						<a href="http://cs.nyu.edu/ijcar2014/" target="_blank">
						IJCAR 2014</a>.
						</i>
						The source code and command-line utility can be downloaded 
						<a href="assets/gen2sat.zip" target="_blank">
						here</a>.
						The web interface was written using
							<img src="assets/playlogo.png" style="height: 1.3em"></a> Framework,
							based on 
							<a href="http://ics-software-engineering.github.io/play-example-form/" target="_blank">
							this
							example</a>.
							<img src="assets/latex.gif" style="height: 1em"> rendering is done with
							<a href="https://www.mathjax.org/" target="_blank">
							 <img src="assets/mathjax.gif" style="height: 1em"></a>.
							
						The SAT-solver that we use is
							<a href="http://www.sat4j.org/index.php" target="_blank">
							sat4j 
							</a>.
						The tool was implemented by 
						<a href="http://cs.tau.ac.il/research/yoni.zohar/" target="_blank">
						Yoni Zohar</a>.
						Any comments or questions are 
						<a href="mailto:yoni.zohar@cs.tau.ac.il">welcome</a>.
					</p>
				</div>
						
		
		
		</div>


""")))}))}
  }

  def render(queryForm:Form[views.formdata.QueryFormData],result:String,bottomLine:String,predefinedSystems:List[String],chosenPredefinedSystem:String,paralyzerOptions:List[String],rulesHeight:String,latexCheckBoxMap:Map[String, Boolean]): play.twirl.api.HtmlFormat.Appendable = apply(queryForm,result,bottomLine,predefinedSystems,chosenPredefinedSystem,paralyzerOptions,rulesHeight,latexCheckBoxMap)

  def f:((Form[views.formdata.QueryFormData],String,String,List[String],String,List[String],String,Map[String, Boolean]) => play.twirl.api.HtmlFormat.Appendable) = (queryForm,result,bottomLine,predefinedSystems,chosenPredefinedSystem,paralyzerOptions,rulesHeight,latexCheckBoxMap) => apply(queryForm,result,bottomLine,predefinedSystems,chosenPredefinedSystem,paralyzerOptions,rulesHeight,latexCheckBoxMap)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Wed Jan 20 14:01:46 IST 2016
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/Index.scala.html
                  HASH: f58f088bb288721562d3bfb9eeff80993cbfa0f7
                  MATRIX: 826->1|1172->244|1200->263|1229->265|1443->451|1472->452|1502->455|1539->464|1568->465|1634->503|1663->504|1691->505|1719->506|1761->522|1783->535|1823->537|1853->540|1964->625|2038->690|2078->692|2111->699|2248->815|2284->821|2315->825|2355->839|2396->871|2436->873|2468->878|2538->922|2591->954|2753->1090|2823->1139|2857->1147|2871->1152|2907->1167|2959->1189|2991->1195|3030->1225|3070->1227|3102->1232|3198->1301|3212->1306|3246->1319|3279->1325|3331->1347|3368->1357
                  LINES: 26->1|35->7|36->9|38->11|43->16|43->16|44->17|44->17|44->17|44->17|44->17|45->18|45->18|48->21|48->21|48->21|49->22|52->25|52->25|52->25|53->26|54->27|55->28|56->29|58->31|58->31|58->31|59->32|60->33|60->33|65->38|65->38|66->39|66->39|66->39|68->41|69->42|69->42|69->42|70->43|71->44|71->44|71->44|72->45|74->47|76->49
                  -- GENERATED --
              */
          