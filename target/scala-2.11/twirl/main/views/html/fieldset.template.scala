
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object fieldset extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[Form[views.formdata.QueryFormData],List[String],String,List[String],String,Map[String, Boolean],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(queryForm: Form[views.formdata.QueryFormData],predefinedSystems: List[String], chosenPredefinedSystem: String, paralyzerOptions: List[String], rulesHeight: String,latexCheckBoxMap:Map[String,Boolean]):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {import views.html.bootstrap3._

Seq[Any](format.raw/*1.203*/("""

"""),format.raw/*4.1*/("""<fieldset id="fs">
  <!-- Header -->
  <div id="legend">
    <legend>Sequent Calculus and Query Sequent</legend>
  </div>
   <div>
	   <div id="wrapperOfButtons">
	   			
	   				<div id="empty"><label class="col-sm-2 control-label"></label></div>
	   				<div id="radios">
					   			 """),_display_(/*14.14*/radiobuttons(queryForm("predefinedSystems"),
						               label = "",
						               buttonNameList = predefinedSystems,
						               //help = "If you want, you could use a predefined calculus",
						               chosen = chosenPredefinedSystem,
						               navOnClick = "true",
						               latex= latexCheckBoxMap.get("latex").toString()          
						         )),format.raw/*21.17*/("""
	   				"""),format.raw/*22.9*/("""</div>
	   				<div id="checkboxLatex" align="right" """),_display_(/*23.48*/{if (chosenPredefinedSystem == "paralyzer" || chosenPredefinedSystem == "" || chosenPredefinedSystem == "blank") {scala.xml.Unparsed("style=\"display:none\"")}}),format.raw/*23.208*/(""">
					   		"""),_display_(/*24.12*/checkboxes(queryForm("latex"),
					   		"",
					   		latexCheckBoxMap,
					   		"",
					   		"true",
					   		chosenPredefinedSystem
					   		)),format.raw/*30.12*/("""
					"""),format.raw/*31.6*/("""</div>
	   </div>
   </div>
  <div id="normalForm" """),_display_(/*34.25*/{if (chosenPredefinedSystem == "paralyzer") {scala.xml.Unparsed("style=\"display:none\"")}}),format.raw/*34.116*/("""> 
 <div>
  """),_display_(/*36.4*/text(queryForm("propositionalConnectives"),
        label = "Connectives",
        placeholder = "Please enter all the propositional connectives you will use, with their arities.")),format.raw/*38.106*/("""
       
 """),format.raw/*40.2*/("""</div>
  """),_display_(/*41.4*/text(queryForm("nextOperators"),
        label = "Next Operators",
        placeholder = "Please enter all Next operators you will use.")),format.raw/*43.71*/("""


  """),_display_(/*46.4*/textarea(queryForm("rules"),
        label = "Rules",
        placeholder = "Please enter all the rules of the system.",
        height=rulesHeight)),format.raw/*49.28*/("""
        
	"""),_display_(/*51.3*/text(queryForm("analyticityWrtTo"),
        label = "Analytic w.r.t.",
        placeholder = "If the calculus is analytic, leave empty.")),format.raw/*53.67*/("""
"""),format.raw/*54.1*/("""</div>

<div id="paraNormalForm" """),_display_(/*56.27*/{if (chosenPredefinedSystem != "paralyzer") {scala.xml.Unparsed("style=\"display:none\"")}}),format.raw/*56.118*/(""">
  """),_display_(/*57.4*/textarea(queryForm("fromParalyzer"),
        label = "Import from Paralyzer",
        placeholder = "Paste the output of Paralyzer here...",
        visible = "false",
        autoadjust = "false",
        help="<a href=\"http://www.logic.at/tinc/webparalyzer/paralyzer.html\" target=\"_blank\"> <font size=4em><b>Go to Paralyzer homepage</b></font></a>"
        )),format.raw/*63.10*/(""" 
  
  """),_display_(/*65.4*/radiobuttons(queryForm("chosenParalyzerOption"),
      label = "Did you begin with LK+ or BK?",
      buttonNameList = paralyzerOptions
      )),format.raw/*68.8*/("""
        
"""),format.raw/*70.1*/("""</div>

        
   """),_display_(/*73.5*/text(queryForm("sequent"),
        label = "Sequent",
        placeholder = "Please enter the sequent.")),format.raw/*75.51*/("""
 
	"""),_display_(/*77.3*/text(queryForm("timeout"),
	label = "Time Limit (sec)",
	placeholder = "Please enter a time limit in seconds (default is 10).")),format.raw/*79.72*/("""
  


  """),format.raw/*83.3*/("""<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button id="submit" type="submit" value="Submit" class="btn btn-primary" formaction="?id="""),_display_(/*85.97*/chosenPredefinedSystem),format.raw/*85.119*/("""">Submit</button>
    </div>
  </div>
</fieldset>

"""))}
  }

  def render(queryForm:Form[views.formdata.QueryFormData],predefinedSystems:List[String],chosenPredefinedSystem:String,paralyzerOptions:List[String],rulesHeight:String,latexCheckBoxMap:Map[String, Boolean]): play.twirl.api.HtmlFormat.Appendable = apply(queryForm,predefinedSystems,chosenPredefinedSystem,paralyzerOptions,rulesHeight,latexCheckBoxMap)

  def f:((Form[views.formdata.QueryFormData],List[String],String,List[String],String,Map[String, Boolean]) => play.twirl.api.HtmlFormat.Appendable) = (queryForm,predefinedSystems,chosenPredefinedSystem,paralyzerOptions,rulesHeight,latexCheckBoxMap) => apply(queryForm,predefinedSystems,chosenPredefinedSystem,paralyzerOptions,rulesHeight,latexCheckBoxMap)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/fieldset.scala.html
                  HASH: bae5b405b46f4025de0f6f65f2acf254483a720b
                  MATRIX: 815->1|1135->202|1163->236|1477->523|1906->931|1942->940|2023->994|2205->1154|2245->1167|2415->1316|2448->1322|2527->1374|2640->1465|2679->1478|2881->1658|2918->1668|2954->1678|3112->1815|3144->1821|3313->1969|3351->1981|3509->2118|3537->2119|3598->2153|3711->2244|3742->2249|4127->2613|4161->2621|4324->2764|4361->2774|4408->2795|4533->2899|4564->2904|4712->3031|4747->3039|4939->3204|4983->3226
                  LINES: 26->1|29->1|31->4|41->14|48->21|49->22|50->23|50->23|51->24|57->30|58->31|61->34|61->34|63->36|65->38|67->40|68->41|70->43|73->46|76->49|78->51|80->53|81->54|83->56|83->56|84->57|90->63|92->65|95->68|97->70|100->73|102->75|104->77|106->79|110->83|112->85|112->85
                  -- GENERATED --
              */
          