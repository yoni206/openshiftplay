
package views.html.bootstrap3

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object radiobuttons extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[Field,String,List[String],String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(field: Field, label: String = "CHANGEME", buttonNameList: List[String], help: String = "", chosen:String="", navOnClick: String = "", latex:String="false"):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.158*/("""

  """),format.raw/*3.3*/("""<div class="form-group """),_display_(/*3.27*/if(field.hasErrors)/*3.46*/ {_display_(Seq[Any](format.raw/*3.48*/("""has-error""")))}),format.raw/*3.58*/("""">
    """),_display_(/*4.6*/{ if (!label.isEmpty()) {
    <label class="col-sm-2 control-label" for="@field.id">
    		{label}
    </label>
	    }
    }),format.raw/*9.6*/(""" 
    """),format.raw/*10.5*/("""<div id=""""),_display_(/*10.15*/(field.id + "s")),format.raw/*10.31*/("""" class="col-sm-10" >
      """),_display_(/*11.8*/for(buttonName <- buttonNameList) yield /*11.41*/ {_display_(Seq[Any](format.raw/*11.43*/("""
      """),format.raw/*12.7*/("""<label class="radio-inline">
        <input
          type="radio"
          name=""""),_display_(/*15.18*/field/*15.23*/.name),format.raw/*15.28*/(""""
          id=""""),_display_(/*16.16*/buttonName),format.raw/*16.26*/(""""
          value=""""),_display_(/*17.19*/buttonName),format.raw/*17.29*/(""""
          """),_display_(/*18.12*/{if (navOnClick=="true") {scala.xml.Unparsed("onclick=" + "\"" + "window.location=" + "'" + "?id="  + buttonName + 
          "&latex=" + latex + 
          "'" + "\"")}}),format.raw/*20.24*/("""
          """),_display_(/*21.12*/if(buttonName == chosen)/*21.36*/ {_display_(Seq[Any](format.raw/*21.38*/("""checked""")))}),format.raw/*21.46*/("""
          """),_display_(/*22.12*/if(buttonName == field.value.getOrElse(""))/*22.55*/ {_display_(Seq[Any](format.raw/*22.57*/("""checked""")))}),format.raw/*22.65*/("""
           """),format.raw/*23.12*/("""/>
        """),_display_(/*24.10*/buttonName),format.raw/*24.20*/("""
      """),format.raw/*25.7*/("""</label>
      """)))}),format.raw/*26.8*/("""
      """),format.raw/*27.7*/("""<span class="help-block">"""),_display_(/*27.33*/help),format.raw/*27.37*/("""</span>
      <span class="help-block">"""),_display_(/*28.33*/{field.error.map { error => error.message }}),format.raw/*28.77*/("""</span>
    </div>
  </div>
"""))}
  }

  def render(field:Field,label:String,buttonNameList:List[String],help:String,chosen:String,navOnClick:String,latex:String): play.twirl.api.HtmlFormat.Appendable = apply(field,label,buttonNameList,help,chosen,navOnClick,latex)

  def f:((Field,String,List[String],String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (field,label,buttonNameList,help,chosen,navOnClick,latex) => apply(field,label,buttonNameList,help,chosen,navOnClick,latex)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/bootstrap3/radiobuttons.scala.html
                  HASH: 39eff20bce5b66af7b496c9bc713609338b79354
                  MATRIX: 788->1|1033->157|1063->161|1113->185|1140->204|1179->206|1219->216|1252->224|1395->348|1428->354|1465->364|1502->380|1557->409|1606->442|1646->444|1680->451|1791->535|1805->540|1831->545|1875->562|1906->572|1953->592|1984->602|2024->615|2215->785|2254->797|2287->821|2327->823|2366->831|2405->843|2457->886|2497->888|2536->896|2576->908|2615->920|2646->930|2680->937|2726->953|2760->960|2813->986|2838->990|2905->1030|2970->1074
                  LINES: 26->1|29->1|31->3|31->3|31->3|31->3|31->3|32->4|37->9|38->10|38->10|38->10|39->11|39->11|39->11|40->12|43->15|43->15|43->15|44->16|44->16|45->17|45->17|46->18|48->20|49->21|49->21|49->21|49->21|50->22|50->22|50->22|50->22|51->23|52->24|52->24|53->25|54->26|55->27|55->27|55->27|56->28|56->28
                  -- GENERATED --
              */
          