
package views.html.bootstrap3

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object radiobuttonsLatexCheckBox extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template7[Field,String,List[String],String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(field: Field, label: String = "CHANGEME", buttonNameList: List[String], help: String = "", chosen:String="", navOnClick: String = "", latex:String="false"):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.158*/("""

  """),format.raw/*3.3*/("""<div class="form-group """),_display_(/*3.27*/if(field.hasErrors)/*3.46*/ {_display_(Seq[Any](format.raw/*3.48*/("""has-error""")))}),format.raw/*3.58*/("""">
    <label class="col-sm-2 control-label" for=""""),_display_(/*4.49*/field/*4.54*/.id),format.raw/*4.57*/("""">
    """),_display_(/*5.6*/{
    	label
    }),format.raw/*7.6*/("""
    """),format.raw/*8.5*/("""</label>
    <div id=""""),_display_(/*9.15*/(field.id + "s")),format.raw/*9.31*/("""" class="col-sm-10" >
      """),_display_(/*10.8*/for(buttonName <- buttonNameList) yield /*10.41*/ {_display_(Seq[Any](format.raw/*10.43*/("""
      """),format.raw/*11.7*/("""<label class="radio-inline">
        <input
          type="radio"
          name=""""),_display_(/*14.18*/field/*14.23*/.name),format.raw/*14.28*/(""""
          id=""""),_display_(/*15.16*/buttonName),format.raw/*15.26*/(""""
          value=""""),_display_(/*16.19*/buttonName),format.raw/*16.29*/(""""
          """),_display_(/*17.12*/{if (navOnClick=="true") {scala.xml.Unparsed("onclick=" + "\"" + "window.location=" + "'" + "?id="  + buttonName + 
          "&latex=" + latex + 
          "'" + "\"")}}),format.raw/*19.24*/("""
          """),_display_(/*20.12*/if(buttonName == chosen)/*20.36*/ {_display_(Seq[Any](format.raw/*20.38*/("""checked""")))}),format.raw/*20.46*/("""
          """),_display_(/*21.12*/if(buttonName == field.value.getOrElse(""))/*21.55*/ {_display_(Seq[Any](format.raw/*21.57*/("""checked""")))}),format.raw/*21.65*/("""
           """),format.raw/*22.12*/("""/>
        """),_display_(/*23.10*/buttonName),format.raw/*23.20*/("""
      """),format.raw/*24.7*/("""</label>
      """)))}),format.raw/*25.8*/("""
      """),format.raw/*26.7*/("""<span class="help-block">"""),_display_(/*26.33*/help),format.raw/*26.37*/("""</span>
      <span class="help-block">"""),_display_(/*27.33*/{field.error.map { error => error.message }}),format.raw/*27.77*/("""</span>
    </div>
  </div>
"""))}
  }

  def render(field:Field,label:String,buttonNameList:List[String],help:String,chosen:String,navOnClick:String,latex:String): play.twirl.api.HtmlFormat.Appendable = apply(field,label,buttonNameList,help,chosen,navOnClick,latex)

  def f:((Field,String,List[String],String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (field,label,buttonNameList,help,chosen,navOnClick,latex) => apply(field,label,buttonNameList,help,chosen,navOnClick,latex)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/bootstrap3/radiobuttonsLatexCheckBox.scala.html
                  HASH: 2e6f5b6c97741082ecb3e06236ca3b7922683d62
                  MATRIX: 801->1|1046->157|1076->161|1126->185|1153->204|1192->206|1232->216|1309->267|1322->272|1345->275|1378->283|1415->301|1446->306|1495->329|1531->345|1586->374|1635->407|1675->409|1709->416|1820->500|1834->505|1860->510|1904->527|1935->537|1982->557|2013->567|2053->580|2244->750|2283->762|2316->786|2356->788|2395->796|2434->808|2486->851|2526->853|2565->861|2605->873|2644->885|2675->895|2709->902|2755->918|2789->925|2842->951|2867->955|2934->995|2999->1039
                  LINES: 26->1|29->1|31->3|31->3|31->3|31->3|31->3|32->4|32->4|32->4|33->5|35->7|36->8|37->9|37->9|38->10|38->10|38->10|39->11|42->14|42->14|42->14|43->15|43->15|44->16|44->16|45->17|47->19|48->20|48->20|48->20|48->20|49->21|49->21|49->21|49->21|50->22|51->23|51->23|52->24|53->25|54->26|54->26|54->26|55->27|55->27
                  -- GENERATED --
              */
          