
package views.html.bootstrap3

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object textarea extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template8[Field,String,String,String,String,String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(field: Field, rows: String = "3", label: String = "CHANGEME", placeholder: String = "", help: String = "", visible: String = "true", height: String = "15", autoadjust : String = "true"):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.188*/("""




  """),format.raw/*6.3*/("""<div class="form-group """),_display_(/*6.27*/if(field.hasErrors)/*6.46*/ {_display_(Seq[Any](format.raw/*6.48*/("""has-error""")))}),format.raw/*6.58*/("""">
    <label class="col-sm-2 control-label">"""),_display_(/*7.44*/label),format.raw/*7.49*/("""</label>
    <div class="col-sm-10">
      <span class="help-block">"""),_display_(/*9.33*/{scala.xml.Unparsed(help)}),format.raw/*9.59*/("""</span>
		<textarea class="form-control" 
				"""),_display_(/*11.6*/{if(autoadjust.equals("true")) {
					"style=overflow:hidden"
				} else {
				"style=data-role:none"
				}}),format.raw/*15.7*/("""
				"""),format.raw/*16.5*/("""rows=""""),_display_(/*16.12*/height),format.raw/*16.18*/(""""
				onkeyup=""""),_display_(/*17.15*/if(autoadjust.equals("true"))/*17.44*/ {_display_(Seq[Any](format.raw/*17.46*/("""
					"""),format.raw/*18.6*/("""this.style.height = this.scrollHeight;
				""")))}),format.raw/*19.6*/("""" 
		id=""""),_display_(/*20.8*/field/*20.13*/.id),format.raw/*20.16*/("""" 
                name=""""),_display_(/*21.24*/field/*21.29*/.name),format.raw/*21.34*/("""" 
                placeholder=""""),_display_(/*22.31*/placeholder),format.raw/*22.42*/("""" 
                >"""),_display_(/*23.19*/field/*23.24*/.value.getOrElse("")),format.raw/*23.44*/("""</textarea>
      <span class="help-block">"""),_display_(/*24.33*/{field.error.map { error => error.message }}),format.raw/*24.77*/("""</span>
    </div>
  </div>
"""))}
  }

  def render(field:Field,rows:String,label:String,placeholder:String,help:String,visible:String,height:String,autoadjust:String): play.twirl.api.HtmlFormat.Appendable = apply(field,rows,label,placeholder,help,visible,height,autoadjust)

  def f:((Field,String,String,String,String,String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (field,rows,label,placeholder,help,visible,height,autoadjust) => apply(field,rows,label,placeholder,help,visible,height,autoadjust)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/bootstrap3/textarea.scala.html
                  HASH: 17a84502242738397f0ec9a9c2ed3c987c6aeb81
                  MATRIX: 785->1|1060->187|1093->194|1143->218|1170->237|1209->239|1249->249|1321->295|1346->300|1441->369|1487->395|1560->442|1688->550|1720->555|1754->562|1781->568|1824->584|1862->613|1902->615|1935->621|2009->665|2045->675|2059->680|2083->683|2136->709|2150->714|2176->719|2236->752|2268->763|2316->784|2330->789|2371->809|2442->853|2507->897
                  LINES: 26->1|29->1|34->6|34->6|34->6|34->6|34->6|35->7|35->7|37->9|37->9|39->11|43->15|44->16|44->16|44->16|45->17|45->17|45->17|46->18|47->19|48->20|48->20|48->20|49->21|49->21|49->21|50->22|50->22|51->23|51->23|51->23|52->24|52->24
                  -- GENERATED --
              */
          