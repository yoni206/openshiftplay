
package views.html.bootstrap3

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object select extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template5[Field,String,Map[String, Boolean],Boolean,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(field: Field, label: String = "CHANGEME", optionMap: Map[String, Boolean], isMultiple: Boolean, help: String = ""):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.117*/("""

  """),format.raw/*3.3*/("""<div class="form-group """),_display_(/*3.27*/if(field.hasErrors)/*3.46*/ {_display_(Seq[Any](format.raw/*3.48*/("""has-error""")))}),format.raw/*3.58*/("""">
    <label class="col-sm-2 control-label" for=""""),_display_(/*4.49*/field/*4.54*/.id),format.raw/*4.57*/("""">"""),_display_(/*4.60*/label),format.raw/*4.65*/("""</label>
    <div class="col-sm-10">
      <select
        class="form-control"
        id=""""),_display_(/*8.14*/field/*8.19*/.id),format.raw/*8.22*/(""""
        name=""""),_display_(/*9.16*/if(isMultiple)/*9.30*/ {_display_(_display_(/*9.33*/(field.name + "[]")))}/*9.54*/else/*9.59*/{_display_(_display_(/*9.61*/field/*9.66*/.name))}),format.raw/*9.72*/(""""
        """),_display_(/*10.10*/if(isMultiple)/*10.24*/ {_display_(Seq[Any](format.raw/*10.26*/("""multiple="multiple"""")))}),format.raw/*10.46*/(""">
        """),_display_(/*11.10*/if(!isMultiple)/*11.25*/ {_display_(Seq[Any](format.raw/*11.27*/("""<option class="blank" value="">Please select a value</option>""")))}),format.raw/*11.89*/("""
        """),_display_(/*12.10*/for((optionName, isSelected) <- optionMap) yield /*12.52*/ {_display_(Seq[Any](format.raw/*12.54*/("""
          """),format.raw/*13.11*/("""<option id=""""),_display_(/*13.24*/optionName),format.raw/*13.34*/("""" value=""""),_display_(/*13.44*/optionName),format.raw/*13.54*/("""" """),_display_(/*13.57*/if(isSelected)/*13.71*/ {_display_(Seq[Any](format.raw/*13.73*/("""selected""")))}),format.raw/*13.82*/(""">"""),_display_(/*13.84*/optionName),format.raw/*13.94*/("""</option>
        """)))}),format.raw/*14.10*/("""
      """),format.raw/*15.7*/("""</select>
      <span class="help-block">"""),_display_(/*16.33*/help),format.raw/*16.37*/("""</span>
      <span class="help-block">"""),_display_(/*17.33*/{field.error.map { error => error.message }}),format.raw/*17.77*/("""</span>
    </div>
  </div>
"""))}
  }

  def render(field:Field,label:String,optionMap:Map[String, Boolean],isMultiple:Boolean,help:String): play.twirl.api.HtmlFormat.Appendable = apply(field,label,optionMap,isMultiple,help)

  def f:((Field,String,Map[String, Boolean],Boolean,String) => play.twirl.api.HtmlFormat.Appendable) = (field,label,optionMap,isMultiple,help) => apply(field,label,optionMap,isMultiple,help)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/bootstrap3/select.scala.html
                  HASH: 5deb95900cf36eebc9fc88f119782b078cc4c482
                  MATRIX: 777->1|981->116|1011->120|1061->144|1088->163|1127->165|1167->175|1244->226|1257->231|1280->234|1309->237|1334->242|1453->335|1466->340|1489->343|1532->360|1554->374|1584->377|1614->398|1626->403|1655->405|1668->410|1696->416|1734->427|1757->441|1797->443|1848->463|1886->474|1910->489|1950->491|2043->553|2080->563|2138->605|2178->607|2217->618|2257->631|2288->641|2325->651|2356->661|2386->664|2409->678|2449->680|2489->689|2518->691|2549->701|2599->720|2633->727|2702->769|2727->773|2794->813|2859->857
                  LINES: 26->1|29->1|31->3|31->3|31->3|31->3|31->3|32->4|32->4|32->4|32->4|32->4|36->8|36->8|36->8|37->9|37->9|37->9|37->9|37->9|37->9|37->9|37->9|38->10|38->10|38->10|38->10|39->11|39->11|39->11|39->11|40->12|40->12|40->12|41->13|41->13|41->13|41->13|41->13|41->13|41->13|41->13|41->13|41->13|41->13|42->14|43->15|44->16|44->16|45->17|45->17
                  -- GENERATED --
              */
          