
package views.html.bootstrap3

import play.twirl.api._
import play.twirl.api.TemplateMagic._

import play.api.templates.PlayMagic._
import models._
import controllers._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.api.i18n._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._
import views.html._

/**/
object checkboxes extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template6[Field,String,Map[String, Boolean],String,String,String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(field: Field, label: String = "", checkboxMap: Map[String, Boolean], help:String = "", navOnClick:String="true", id:String="blank"):play.twirl.api.HtmlFormat.Appendable = {
      _display_ {

Seq[Any](format.raw/*1.134*/("""

  """),format.raw/*3.3*/("""<div class="form-group """),_display_(/*3.27*/if(field.hasErrors)/*3.46*/ {_display_(Seq[Any](format.raw/*3.48*/("""has-error""")))}),format.raw/*3.58*/("""">
    <label class="col-sm-2 control-label" for=""""),_display_(/*4.49*/field/*4.54*/.id),format.raw/*4.57*/("""">"""),_display_(/*4.60*/label),format.raw/*4.65*/("""</label>
    <div class="col-sm-10" id=""""),_display_(/*5.33*/field/*5.38*/.id),format.raw/*5.41*/("""">
      """),_display_(/*6.8*/for((checkboxName, isChecked) <- checkboxMap) yield /*6.53*/ {_display_(Seq[Any](format.raw/*6.55*/("""
        """),format.raw/*7.9*/("""<label class="checkbox-inline">
          <input
            type="checkbox"
            name=""""),_display_(/*10.20*/(field.name + "[]")),format.raw/*10.39*/(""""   
            id=""""),_display_(/*11.18*/checkboxName),format.raw/*11.30*/(""""
            value=""""),_display_(/*12.21*/checkboxName),format.raw/*12.33*/(""""
			"""),_display_(/*13.5*/{if (navOnClick=="true") {scala.xml.Unparsed("onclick=" + "\"" + "window.location=" + "'" + "?id="  + id + 
			"&latex=" +
			!isChecked
			+
			"'" + "\"")}}),format.raw/*17.17*/("""
            """),_display_(/*18.14*/if(isChecked)/*18.27*/ {_display_(Seq[Any](format.raw/*18.29*/("""checked""")))}),format.raw/*18.37*/(""">
          """),_display_(/*19.12*/if(checkboxName=="latex")/*19.37*/{_display_(Seq[Any](format.raw/*19.38*/("""<img src="assets/latex.gif">""")))}/*19.68*/else/*19.73*/{_display_(Seq[Any](format.raw/*19.74*/("""checkboxName""")))}),format.raw/*19.87*/("""
        """),format.raw/*20.9*/("""</label>
      """)))}),format.raw/*21.8*/("""
      """),format.raw/*22.7*/("""<span class="help-block">"""),_display_(/*22.33*/help),format.raw/*22.37*/("""</span>
      <span class="help-block">"""),_display_(/*23.33*/{field.error.map { error => error.message }}),format.raw/*23.77*/("""</span>
    </div>
  </div>
"""))}
  }

  def render(field:Field,label:String,checkboxMap:Map[String, Boolean],help:String,navOnClick:String,id:String): play.twirl.api.HtmlFormat.Appendable = apply(field,label,checkboxMap,help,navOnClick,id)

  def f:((Field,String,Map[String, Boolean],String,String,String) => play.twirl.api.HtmlFormat.Appendable) = (field,label,checkboxMap,help,navOnClick,id) => apply(field,label,checkboxMap,help,navOnClick,id)

  def ref: this.type = this

}
              /*
                  -- GENERATED --
                  DATE: Fri Nov 20 07:40:38 IST 2015
                  SOURCE: /Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/app/views/bootstrap3/checkboxes.scala.html
                  HASH: c6b711646f170db0dcb3db1c65343ecdc46c0af2
                  MATRIX: 787->1|1008->133|1038->137|1088->161|1115->180|1154->182|1194->192|1271->243|1284->248|1307->251|1336->254|1361->259|1428->300|1441->305|1464->308|1499->318|1559->363|1598->365|1633->374|1756->470|1796->489|1845->511|1878->523|1927->545|1960->557|1992->563|2171->721|2212->735|2234->748|2274->750|2313->758|2353->771|2387->796|2426->797|2474->827|2487->832|2526->833|2570->846|2606->855|2652->871|2686->878|2739->904|2764->908|2831->948|2896->992
                  LINES: 26->1|29->1|31->3|31->3|31->3|31->3|31->3|32->4|32->4|32->4|32->4|32->4|33->5|33->5|33->5|34->6|34->6|34->6|35->7|38->10|38->10|39->11|39->11|40->12|40->12|41->13|45->17|46->18|46->18|46->18|46->18|47->19|47->19|47->19|47->19|47->19|47->19|47->19|48->20|49->21|50->22|50->22|50->22|51->23|51->23
                  -- GENERATED --
              */
          