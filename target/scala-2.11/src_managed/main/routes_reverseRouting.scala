// @SOURCE:/Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/conf/routes
// @HASH:43ebe1b762d5e15abadd516fe1081984c56acbc1
// @DATE:Fri Nov 20 07:40:36 IST 2015

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString


// @LINE:10
// @LINE:7
// @LINE:6
package controllers {

// @LINE:10
class ReverseAssets {


// @LINE:10
def at(file:String): Call = {
   implicit val _rrc = new ReverseRouteContext(Map(("path", "/public")))
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                        

}
                          

// @LINE:7
// @LINE:6
class ReverseApplication {


// @LINE:6
def getIndex(id:String = "", latex:String = ""): Call = {
   import ReverseRouteContext.empty
   Call("GET", _prefix + queryString(List(if(id == "") None else Some(implicitly[QueryStringBindable[String]].unbind("id", id)), if(latex == "") None else Some(implicitly[QueryStringBindable[String]].unbind("latex", latex)))))
}
                        

// @LINE:7
def postIndex(id:String = "", latex:String = ""): Call = {
   import ReverseRouteContext.empty
   Call("POST", _prefix + queryString(List(if(id == "") None else Some(implicitly[QueryStringBindable[String]].unbind("id", id)), if(latex == "") None else Some(implicitly[QueryStringBindable[String]].unbind("latex", latex)))))
}
                        

}
                          
}
                  


// @LINE:10
// @LINE:7
// @LINE:6
package controllers.javascript {
import ReverseRouteContext.empty

// @LINE:10
class ReverseAssets {


// @LINE:10
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        

}
              

// @LINE:7
// @LINE:6
class ReverseApplication {


// @LINE:6
def getIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.getIndex",
   """
      function(id,latex) {
      return _wA({method:"GET", url:"""" + _prefix + """" + _qS([(id == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("id", id)), (latex == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("latex", latex))])})
      }
   """
)
                        

// @LINE:7
def postIndex : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Application.postIndex",
   """
      function(id,latex) {
      return _wA({method:"POST", url:"""" + _prefix + """" + _qS([(id == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("id", id)), (latex == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("latex", latex))])})
      }
   """
)
                        

}
              
}
        


// @LINE:10
// @LINE:7
// @LINE:6
package controllers.ref {


// @LINE:10
class ReverseAssets {


// @LINE:10
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """ Map static resources from the /public folder to the /assets URL path""", _prefix + """assets/$file<.+>""")
)
                      

}
                          

// @LINE:7
// @LINE:6
class ReverseApplication {


// @LINE:6
def getIndex(id:String, latex:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.getIndex(id, latex), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "getIndex", Seq(classOf[String], classOf[String]), "GET", """ Home page""", _prefix + """""")
)
                      

// @LINE:7
def postIndex(id:String, latex:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Application.postIndex(id, latex), HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "postIndex", Seq(classOf[String], classOf[String]), "POST", """""", _prefix + """""")
)
                      

}
                          
}
        
    