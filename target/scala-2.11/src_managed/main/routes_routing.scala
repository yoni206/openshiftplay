// @SOURCE:/Users/yonizohar/Dropbox/eclipse-ws-for-play/satBasedDecProc/conf/routes
// @HASH:43ebe1b762d5e15abadd516fe1081984c56acbc1
// @DATE:Fri Nov 20 07:40:36 IST 2015


import play.core._
import play.core.Router._
import play.core.Router.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._
import _root_.controllers.Assets.Asset
import _root_.play.libs.F

import Router.queryString

object Routes extends Router.Routes {

import ReverseRouteContext.empty

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:6
private[this] lazy val controllers_Application_getIndex0_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_Application_getIndex0_invoker = createInvoker(
controllers.Application.getIndex(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "getIndex", Seq(classOf[String], classOf[String]),"GET", """ Home page""", Routes.prefix + """"""))
        

// @LINE:7
private[this] lazy val controllers_Application_postIndex1_route = Route("POST", PathPattern(List(StaticPart(Routes.prefix))))
private[this] lazy val controllers_Application_postIndex1_invoker = createInvoker(
controllers.Application.postIndex(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Application", "postIndex", Seq(classOf[String], classOf[String]),"POST", """""", Routes.prefix + """"""))
        

// @LINE:10
private[this] lazy val controllers_Assets_at2_route = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
private[this] lazy val controllers_Assets_at2_invoker = createInvoker(
controllers.Assets.at(fakeValue[String], fakeValue[String]),
HandlerDef(this.getClass.getClassLoader, "", "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """ Map static resources from the /public folder to the /assets URL path""", Routes.prefix + """assets/$file<.+>"""))
        
def documentation = List(("""GET""", prefix,"""controllers.Application.getIndex(id:String ?= "", latex:String ?= "")"""),("""POST""", prefix,"""controllers.Application.postIndex(id:String ?= "", latex:String ?= "")"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]]
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:6
case controllers_Application_getIndex0_route(params) => {
   call(params.fromQuery[String]("id", Some("")), params.fromQuery[String]("latex", Some(""))) { (id, latex) =>
        controllers_Application_getIndex0_invoker.call(controllers.Application.getIndex(id, latex))
   }
}
        

// @LINE:7
case controllers_Application_postIndex1_route(params) => {
   call(params.fromQuery[String]("id", Some("")), params.fromQuery[String]("latex", Some(""))) { (id, latex) =>
        controllers_Application_postIndex1_invoker.call(controllers.Application.postIndex(id, latex))
   }
}
        

// @LINE:10
case controllers_Assets_at2_route(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        controllers_Assets_at2_invoker.call(controllers.Assets.at(path, file))
   }
}
        
}

}
     