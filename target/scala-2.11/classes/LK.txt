name: LK
displayName: Classical Logic
connectives: AND:2,OR:2,IMPLIES:2,NOT:1,TOP:0
rule: =>p1; =>p2 / => p1 AND p2
rule: p1,p2=> /  p1 AND p2 =>
rule: =>p1,p2 / => p1 OR p2
rule: p1=> ;p2=> / p1 OR p2 =>
rule: p1 => p2 / =>p1 IMPLIES p2
rule: =>p1; p2=> / p1 IMPLIES p2 =>
rule: =>p1 / NOT p1 =>
rule: p1 => / =>NOT p1
rule: / =>TOP
analyticity: 
inputSequent: p1, p2=> p1 AND p2