import static org.fest.assertions.Assertions.assertThat; 

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import models.DecisionProcedure;
import models.DecisionProperties;
import models.DecisionResult;
import models.Formula;
import models.Language;
import models.PropositionalConnective;
import models.Sequent;
import models.SequentCalculus;

import org.junit.Before;
import org.junit.Test;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.TimeoutException;

import play.core.StaticApplication;
import utils.CollectionUtils;
import utils.FileUtilsInternal;
import utils.StringUtils;
import views.formdata.QueryFormData;



/**
 *
 * Simple (JUnit) tests that can call all parts of a play app.
 * If you are interested in mocking a whole application, see the wiki for more details.
 *
 */
public class ApplicationTest {

	@Before
	public void loadPredefinedSystems() throws IOException {
		new StaticApplication(new File("")).application();
		QueryFormData.addPredefinedSystems();
	}


	@Test
	public void simpleCheck() {
		int a = 1 + 1;
		assertThat(a).isEqualTo(2);
	}



	@Test
	public void yasc() {
		assertThat("a").isEqualTo("a");
	}



	@Test
	public void localFormulasNoPrefixNoUnariesComplex() {
		List<String> originalFormulasStrings = new ArrayList<String>();
		List<String> localFormulasStrings = new ArrayList<String>();
		originalFormulasStrings.add("(p1 AND (p2 AND ~p2)) IMPLIES (p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("(p1 AND (p2 AND ~p2)) IMPLIES (p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("p1 AND (p2 AND ~p2)");
		localFormulasStrings.add("p1");
		localFormulasStrings.add("(p2 AND ~p2)");
		localFormulasStrings.add("p2");
		localFormulasStrings.add("~p2");
		localFormulasStrings.add("(p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("p3");
		localFormulasStrings.add("~(p3 OR p3)");
		localFormulasStrings.add("(p3 OR p3)");
		Language L = Language.makeInstance("", "AND:2,OR:2,IMPLIES:2,~:1", "");
		Set<Formula> originalFormulas = createFormulas(originalFormulasStrings, L);
		Set<Formula> appLocalFormulas = new Sequent((Set<Formula>) new HashSet<Formula>(), (originalFormulas)).getLocalFormulas((Set<PropositionalConnective>) new HashSet<PropositionalConnective>());
		Set<Formula> trueLocalFormulas = createFormulas(localFormulasStrings, L);
		Set<Formula> diff = CollectionUtils.diff(appLocalFormulas, trueLocalFormulas);
		assertThat(diff).isEmpty();    }

	@Test
	public void localFormulasNoPrefixWithUnariesComplex() {
		List<String> originalFormulasStrings = new ArrayList<String>();
		List<String> localFormulasStrings = new ArrayList<String>();
		originalFormulasStrings.add("(p1 AND (p2 AND ~p2)) IMPLIES (p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("(p1 AND (p2 AND ~p2)) IMPLIES (p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("p1 AND (p2 AND ~p2)");
		localFormulasStrings.add("p1");
		localFormulasStrings.add("(p2 AND ~p2)");
		localFormulasStrings.add("p2");
		localFormulasStrings.add("~p2");
		localFormulasStrings.add("(p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("p3");
		localFormulasStrings.add("~(p3 OR p3)");
		localFormulasStrings.add("(p3 OR p3)");

		localFormulasStrings.add("~(p1 AND (p2 AND ~p2))");
		localFormulasStrings.add("~p1");
		localFormulasStrings.add("~(p2 AND ~p2)");
		localFormulasStrings.add("~~p2");
		localFormulasStrings.add("~(p3 OR ~(p3 OR p3))");
		localFormulasStrings.add("~p3");
		localFormulasStrings.add("~~(p3 OR p3)");
		Language L = Language.makeInstance("", "AND:2,OR:2,IMPLIES:2,~:1", "");
		Set<Formula> originalFormulas = createFormulas(originalFormulasStrings, L);
		Set<Formula> appLocalFormulas = new Sequent((Set<Formula>) new HashSet<Formula>(), (originalFormulas)).getLocalFormulas(L.getConnectivesWithArity(1));
		Set<Formula> trueLocalFormulas = createFormulas(localFormulasStrings, L);
		Set<Formula> diff = CollectionUtils.diff(appLocalFormulas, trueLocalFormulas);
		assertThat(diff).isEmpty();    }

	@Test
	public void localFormulasWithPrefixWithoutUnariesComplex1() {
		List<String> originalFormulasStrings = new ArrayList<String>();
		List<String> localFormulasStrings = new ArrayList<String>();
		originalFormulasStrings.add("q1said ((p1 AND (q3said p2 AND ~p2)) IMPLIES q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("q1said ((p1 AND (q3said p2 AND ~p2)) IMPLIES q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("q1said(p1 AND (q3said p2 AND ~p2))");
		localFormulasStrings.add("q1said p1");
		localFormulasStrings.add("q1said (q3 said p2 AND ~p2)");
		localFormulasStrings.add("q1said q3said p2");
		localFormulasStrings.add("q1said~p2");
		localFormulasStrings.add("q1said p2");
		localFormulasStrings.add("q1said q2said(p3 OR q1said~(p3 OR p3))");
		localFormulasStrings.add("q1said q2said p3");
		localFormulasStrings.add("q1said q2said q1said~(p3 OR p3)");
		localFormulasStrings.add("q1said q2said q1said(p3 OR p3)");
		localFormulasStrings.add("q1said q2said q1said p3");
		Language L = Language.makeInstance("", "AND:2,OR:2,IMPLIES:2,~:1", "q1said, q2said, q3said, q4said");
		Set<Formula> originalFormulas = createFormulas(originalFormulasStrings, L);
		Set<Formula> appLocalFormulas = new Sequent((Set<Formula>) new HashSet<Formula>(), (originalFormulas)).getLocalFormulas(new LinkedHashSet<PropositionalConnective>());
		Set<Formula> trueLocalFormulas = createFormulas(localFormulasStrings, L);
		Set<Formula> diff = CollectionUtils.diff(appLocalFormulas, trueLocalFormulas);
		assertThat(diff).isEmpty();    }    


	@Test
	public void localFormulasWithPrefixWithUnariesComplex1() {
		List<String> originalFormulasStrings = new ArrayList<String>();
		List<String> localFormulasStrings = new ArrayList<String>();
		originalFormulasStrings.add("q1said ((p1 AND (q3said p2 AND ~p2)) IMPLIES q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("q1said ((p1 AND (q3said p2 AND ~p2)) IMPLIES q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("q1said(p1 AND (q3said p2 AND ~p2))");
		localFormulasStrings.add("q1said p1");
		localFormulasStrings.add("q1said (q3 said p2 AND ~p2)");
		localFormulasStrings.add("q1said q3said p2");
		localFormulasStrings.add("q1said~p2");
		localFormulasStrings.add("q1said p2");
		localFormulasStrings.add("q1said q2said(p3 OR q1said~(p3 OR p3))");
		localFormulasStrings.add("q1said q2said p3");
		localFormulasStrings.add("q1said q2said q1said~(p3 OR p3)");
		localFormulasStrings.add("q1said q2said q1said(p3 OR p3)");
		localFormulasStrings.add("q1said q2said q1said p3");

		localFormulasStrings.add("q1said~(p1 AND (q3said p2 AND ~p2))");
		localFormulasStrings.add("q1said~p1");
		localFormulasStrings.add("q1said~(q3said p2 AND ~p2)");
		localFormulasStrings.add("q1said~q3said p2");
		localFormulasStrings.add("q1said~~p2");

		localFormulasStrings.add("q1said ~(q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("q1saidq2said ~p3");
		localFormulasStrings.add("q1saidq2said~q1said~(p3 OR p3)");
		localFormulasStrings.add("q1saidq2saidq1said~p3");


		Language L = Language.makeInstance("", "AND:2,OR:2,IMPLIES:2,~:1", "q1said, q2said, q3said, q4said");
		Set<Formula> originalFormulas = createFormulas(originalFormulasStrings, L);
		Set<Formula> appLocalFormulas = new Sequent((Set<Formula>) new HashSet<Formula>(), (originalFormulas)).getLocalFormulas(L.getConnectivesWithArity(1));
		Set<Formula> trueLocalFormulas = createFormulas(localFormulasStrings, L);
		Set<Formula> diff = CollectionUtils.diff(appLocalFormulas, trueLocalFormulas);
		assertThat(diff).isEmpty();
	}    

	@Test
	public void localFormulasWithPrefixWithUnariesComplex2() {
		List<String> originalFormulasStrings = new ArrayList<String>();
		List<String> localFormulasStrings = new ArrayList<String>();
		originalFormulasStrings.add(" ((p1 AND (q3said p2 AND ~p2)) IMPLIES q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add(" ((p1 AND (q3said p2 AND ~p2)) IMPLIES q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("(p1 AND (q3said p2 AND ~p2))");
		localFormulasStrings.add(" p1");
		localFormulasStrings.add(" (q3 said p2 AND ~p2)");
		localFormulasStrings.add(" q3said p2");
		localFormulasStrings.add("~p2");
		localFormulasStrings.add(" p2");
		localFormulasStrings.add(" q2said(p3 OR q1said~(p3 OR p3))");
		localFormulasStrings.add(" q2said p3");
		localFormulasStrings.add(" q2said q1said~(p3 OR p3)");
		localFormulasStrings.add(" q2said q1said(p3 OR p3)");
		localFormulasStrings.add(" q2said q1said p3");

		localFormulasStrings.add("~(p1 AND (q3said p2 AND ~p2))");
		localFormulasStrings.add("~p1");
		localFormulasStrings.add("~(q3said p2 AND ~p2)");
		localFormulasStrings.add("~q3said p2");
		localFormulasStrings.add("~~p2");

		localFormulasStrings.add(" ~(q2said(p3 OR q1said~(p3 OR p3)))");
		localFormulasStrings.add("q2said ~p3");
		localFormulasStrings.add("q2said~q1said~(p3 OR p3)");
		localFormulasStrings.add("q2saidq1said~p3");


		Language L = Language.makeInstance("", "AND:2,OR:2,IMPLIES:2,~:1", "q1said, q2said, q3said, q4said");
		Set<Formula> originalFormulas = createFormulas(originalFormulasStrings, L);
		Set<Formula> appLocalFormulas = new Sequent((Set<Formula>) new HashSet<Formula>(), (originalFormulas)).getLocalFormulas(L.getConnectivesWithArity(1));
		Set<Formula> trueLocalFormulas = createFormulas(localFormulasStrings, L);
		Set<Formula> diff = CollectionUtils.diff(appLocalFormulas, trueLocalFormulas);
		assertThat(diff).isEmpty();
	}    



	@Test
	public void localFormulasWithPrefixWithUnariesComplex2Tex() {
		List<String> originalFormulasStrings = new ArrayList<String>();
		List<String> localFormulasStrings = new ArrayList<String>();
		originalFormulasStrings.add(" ((p1 \\wedge (\\ast_3 p2 \\wedge \\neg p2)) \\rightarrow \\ast_2(p3 \\vee \\ast_1\\neg (p3 \\vee p3)))");
		localFormulasStrings.add(" ((p1 \\wedge (\\ast_3 p2 \\wedge \\neg p2)) \\rightarrow \\ast_2(p3 \\vee \\ast_1\\neg (p3 \\vee p3)))");
		localFormulasStrings.add("(p1 \\wedge (\\ast_3 p2 \\wedge \\neg p2))");
		localFormulasStrings.add(" p1");
		localFormulasStrings.add(" (\\ast_3 p2 \\wedge \\neg p2)");
		localFormulasStrings.add(" \\ast_3 p2");
		localFormulasStrings.add("\\neg p2");
		localFormulasStrings.add(" p2");
		localFormulasStrings.add(" \\ast_2(p3 \\vee \\ast_1\\neg (p3 \\vee p3))");
		localFormulasStrings.add(" \\ast_2 p3");
		localFormulasStrings.add(" \\ast_2 \\ast_1\\neg (p3 \\vee p3)");
		localFormulasStrings.add(" \\ast_2 \\ast_1(p3 \\vee p3)");
		localFormulasStrings.add(" \\ast_2 \\ast_1 p3");

		localFormulasStrings.add("\\neg (p1 \\wedge (\\ast_3 p2 \\wedge \\neg p2))");
		localFormulasStrings.add("\\neg p1");
		localFormulasStrings.add("\\neg (\\ast_3 p2 \\wedge \\neg p2)");
		localFormulasStrings.add("\\neg \\ast_3 p2");
		localFormulasStrings.add("\\neg \\neg p2");

		localFormulasStrings.add(" \\neg (\\ast_2(p3 \\vee \\ast_1\\neg (p3 \\vee p3)))");
		localFormulasStrings.add("\\ast_2 \\neg p3");
		localFormulasStrings.add("\\ast_2\\neg \\ast_1\\neg (p3 \\vee p3)");
		localFormulasStrings.add("\\ast_2\\ast_1\\neg p3");


		Language L = Language.makeInstance("", "\\wedge:2,\\vee:2,\\rightarrow:2,\\neg :1", "\\ast_1, \\ast_2, \\ast_3, \\ast_4");
		Set<Formula> originalFormulas = createFormulas(originalFormulasStrings, L);
		Set<Formula> appLocalFormulas = new Sequent((Set<Formula>) new HashSet<Formula>(), (originalFormulas)).getLocalFormulas(L.getConnectivesWithArity(1));
		Set<Formula> trueLocalFormulas = createFormulas(localFormulasStrings, L);
		Set<Formula> diff = CollectionUtils.diff(appLocalFormulas, trueLocalFormulas);
		assertThat(diff).isEmpty();
	}    


	//    @Test
	//    public void semanticConditions1() {
	//    	Language L = Language.makeInstance("", "AND:2, OR:2, IMPLIES:2, TOP:0", "q1said,q2said,q3said");
	//    	Formula f = Formula.makeInstance(" q2said(p1 OR q1said(p2 OR TOP))", L);
	//    	Set<Formula> formulas = f.getLocalFormulas(L.getConnectivesWithArity(1));
	//    	SequentCalculus G = SequentCalculus.makeInstance(
	//    			QueryFormData.getFamousCalculiFormDatas().get("pil").rules,
	//    			L, 
	//    			"");
	//    	
	//    	Set<String> constraintsStrings = new LinkedHashSet<String>();
	//    	constraintsStrings.add("q2said p1=>q2said (p1 OR q1said(p2 OR TOP))");
	//    	constraintsStrings.add("q2said q1said(p2 OR TOP) =>q2said (p1 OR q1said(p2 OR TOP))");
	//    	constraintsStrings.add("q2saidq1said p2 => q2saidq1said (p2 OR TOP)");
	//    	constraintsStrings.add("q2saidq1said TOP => q2saidq1said (p2 OR TOP)");
	//    	constraintsStrings.add("=>q2saidq1said TOP");
	//    	Set<Sequent> trueConstraints = createSequents(constraintsStrings, L);
	//    	
	//    	Set<Sequent> appConstraints = G.getSemanticConstraints(formulas);
	//    	Set<Sequent> diff = CollectionUtils.diff(appConstraints, trueConstraints);
	//    	assertThat(diff).isEmpty();
	//    	
	//    }

	//    @Test
	//    public void semanticConditions1Tex() {
	//    	Language L = Language.makeInstance("", StringUtils.putDoubleSlashesAndDoubleArrow("\\wedge:2, \\vee:2, \\rightarrow:2, \\top:0"), 
	//    			StringUtils.putDoubleSlashesAndDoubleArrow("\\ast_1,\\ast_2,\\ast_3"));
	//    	Formula f = Formula.makeInstance(StringUtils.putDoubleSlashesAndDoubleArrow(" \\ast_2(p1 \\vee \\ast_1(p2 \\vee \\top))"), L);
	//    	Set<Formula> formulas = f.getLocalFormulas(L.getConnectivesWithArity(1));
	//    	QueryFormData q = QueryFormData.getFormDataOfCalculus("pil", "true");
	//    	q.normalizeTexInput();
	//    	SequentCalculus G = SequentCalculus.makeInstance(
	//    			q.rules,
	//    			L, 
	//    			"");
	//    	
	//    	Set<String> constraintsStrings = new LinkedHashSet<String>();
	//    	constraintsStrings.add(StringUtils.putDoubleSlashesAndDoubleArrow("\\ast_2 p1=>\\ast_2 (p1 \\vee \\ast_1(p2 \\vee \\top))"));
	//    	constraintsStrings.add(StringUtils.putDoubleSlashesAndDoubleArrow("\\ast_2 \\ast_1(p2 \\vee \\top) =>\\ast_2 (p1 \\vee \\ast_1(p2 \\vee \\top))"));
	//    	constraintsStrings.add(StringUtils.putDoubleSlashesAndDoubleArrow("\\ast_2\\ast_1 p2 => \\ast_2\\ast_1 (p2 \\vee \\top)"));
	//    	constraintsStrings.add(StringUtils.putDoubleSlashesAndDoubleArrow("\\ast_2\\ast_1 \\top => \\ast_2\\ast_1 (p2 \\vee \\top)"));
	//    	constraintsStrings.add(StringUtils.putDoubleSlashesAndDoubleArrow("=>\\ast_2\\ast_1 \\top"));
	//    	Set<Sequent> trueConstraints = createSequents(constraintsStrings, L);
	//    	
	//    	Set<Sequent> appConstraints = G.getSemanticConstraints(formulas);
	//    	Set<Sequent> diff = CollectionUtils.diff(appConstraints, trueConstraints);
	//    	assertThat(diff).isEmpty();
	//    	
	//    }    
	//    
	//    @Test
	//    public void semanticConditions2() {
	//    	Language L = Language.makeInstance("", "AND:2, OR:2, IMPLIES:2, ~:1", "");
	//    	Formula f = Formula.makeInstance(" (a AND ~a) OR (~ (a AND ~a))", L);
	//    	Set<Formula> formulas = f.getLocalFormulas(L.getConnectivesWithArity(1));
	//    	SequentCalculus G = SequentCalculus.makeInstance(
	//    			QueryFormData.getFamousCalculiFormDatas().get("c1").rules,
	//    			L, 
	//    			"~");
	//    	
	//    	Set<String> constraintsStrings = new LinkedHashSet<String>();
	//    	constraintsStrings.add("(a AND ~a)=>(a AND ~a) OR (~ (a AND ~a))");
	//    	constraintsStrings.add("~(a AND ~a)=>(a AND ~a) OR (~ (a AND ~a))");
	//    	constraintsStrings.add("(a AND ~a) OR (~ (a AND ~a))=>(a AND ~a) , (~ (a AND ~a))");
	//    	constraintsStrings.add("a AND ~a => a");
	//    	constraintsStrings.add("a AND ~a => ~a");
	//    	constraintsStrings.add("a,~a=>a AND ~a");
	//    	constraintsStrings.add("~ (a AND ~a),a,~a=>");
	//    	constraintsStrings.add("~(a AND ~a)=>~a,~~a");
	//    	constraintsStrings.add("=>~(a AND ~a), a AND ~a");
	//    	constraintsStrings.add("=>~~(a AND ~a), ~(a AND ~a)");
	//    	constraintsStrings.add("=>~a,a");
	//    	constraintsStrings.add("=>~~a,~a");
	//    	constraintsStrings.add("~~a=>a");
	//    	constraintsStrings.add("~~(a AND ~a)=>a AND ~a");
	//    	
	//    	Set<Sequent> trueConstraints = createSequents(constraintsStrings, L);
	//    	
	//    	Set<Sequent> appConstraints = G.getSemanticConstraints(formulas);
	//    	Set<Sequent> diff = CollectionUtils.diff(appConstraints, trueConstraints);
	//    	assertThat(diff).isEmpty();
	//    	
	//    }

	//    @Test
	//    public void semanticConditions3() {
	//    	Language L = Language.makeInstance("", "AND:2, OR:2, IMPLIES:2, ~:1", "");
	//    	Formula f = Formula.makeInstance(" (a AND ~a) OR (~ (a AND ~a))", L);
	//    	Set<Formula> formulas = f.getLocalFormulas(L.getConnectivesWithArity(1));
	//    	SequentCalculus G = SequentCalculus.makeInstance(
	//    			QueryFormData.getFamousCalculiFormDatas().get("c1").rules,
	//    			L, 
	//    			"~");
	//    	
	//    	Set<String> constraintsStrings = new LinkedHashSet<String>();
	//    	constraintsStrings.add("(a AND ~a)=>(a AND ~a) OR (~ (a AND ~a))");
	//    	constraintsStrings.add("~(a AND ~a)=>(a AND ~a) OR (~ (a AND ~a))");
	//    	constraintsStrings.add("(a AND ~a) OR (~ (a AND ~a))=>(a AND ~a) , (~ (a AND ~a))");
	//    	constraintsStrings.add("a AND ~a => a");
	//    	constraintsStrings.add("a AND ~a => ~a");
	//    	constraintsStrings.add("a,~a=>a AND ~a");
	//    	constraintsStrings.add("~ (a AND ~a),a,~a=>");
	//    	constraintsStrings.add("~(a AND ~a)=>~a,~~a");
	//    	constraintsStrings.add("=>~(a AND ~a), a AND ~a");
	//    	constraintsStrings.add("=>~~(a AND ~a), ~(a AND ~a)");
	//    	constraintsStrings.add("=>~a,a");
	//    	constraintsStrings.add("=>~~a,~a");
	//    	constraintsStrings.add("~~a=>a");
	//    	constraintsStrings.add("~~(a AND ~a)=>a AND ~a");
	//    	
	//    	Set<Sequent> trueConstraints = createSequents(constraintsStrings, L);
	//    	
	//    	Set<Sequent> appConstraints = G.getSemanticConstraints(formulas);
	//    	Set<Sequent> diff = CollectionUtils.diff(appConstraints, trueConstraints);
	//    	assertThat(diff).isEmpty();
	//    	
	//    }
	//
	//
	//    @Test
	//    public void decide1() throws ContradictionException, TimeoutException {
	//    	Language L = Language.makeInstance("", "AND:2, OR:2, IMPLIES:2, ~:1", "");
	//    	String formula = " (a AND ~a) OR (~ (a AND ~a))";
	//    	SequentCalculus G = SequentCalculus.makeInstance(
	//    			QueryFormData.getFamousCalculiFormDatas().get("c1").rules,
	//    			L, 
	//    			"~");
	//    	DecisionProcedure d = new DecisionProcedure(G,	10);
	//    	assertThat(d.findRefutingVal(Sequent.makeInstance("=>" + formula, L))).isNull();
	//
	//    }
	//    
	//    @Test
	//    public void decide2() throws ContradictionException, TimeoutException {
	//    	Language L = Language.makeInstance("", "AND:2, OR:2, IMPLIES:2, ~:1", "");
	//    	String formula = " (a AND ~a) OR (~ (a AND ~a))";
	//    	SequentCalculus G = SequentCalculus.makeInstance(
	//    			QueryFormData.getFamousCalculiFormDatas().get("c1").rules,
	//    			L, 
	//    			"~");
	//    	DecisionProcedure d = new DecisionProcedure(G,	10);
	//    	assertThat(d.findRefutingVal(Sequent.makeInstance(formula + "=>", L))).isNotNull();
	//
	//    }
	//

	@Test
	public void orisExample() throws ContradictionException, TimeoutException {
		boolean pass;
		QueryFormData q = new QueryFormData(QueryFormData.getFormDataOfCalculus("c1"));
		q.sequent = "NOT p1, NOT p2, NOT p1 AND NOT p2, NOT (NOT p1 AND NOT p2) => p1, p2, NOT NOT p1";
		pass = runFromQueryFormData(q, true);
		assertThat(pass).isTrue();
		q.analyticityWrtTo = "";
		pass = runFromQueryFormData(q, false);
		assertThat(pass).isTrue();
	}

	@Test
	public void orisExampleTex() throws ContradictionException, TimeoutException {
		boolean pass;
		QueryFormData q = new QueryFormData(QueryFormData.getFormDataOfCalculus("c1"));
		q.sequent = "NOT p1, NOT p2, NOT p1 AND NOT p2, NOT (NOT p1 AND NOT p2) => p1, p2, NOT NOT p1";
		q.texify();
		pass = runFromQueryFormData(q, true);
		assertThat(pass).isTrue();
		q.analyticityWrtTo = "";
		pass = runFromQueryFormData(q, false);
		assertThat(pass).isTrue();
	}    

	@Test
	public void decideFromFile() throws ContradictionException, TimeoutException, IOException {
		String testsDataString = FileUtilsInternal.getFileAsString("conf/data.txt");
		List<String> requiredTestsStrings = StringUtils.splitAndTrimToList(testsDataString, "\n\n");
		for (String requiredTest : requiredTestsStrings) {
			boolean pass;
			pass = runTest(requiredTest, "false");
			assertThat(pass).isTrue();
			pass = runTest(requiredTest, "true");
			assertThat(pass).isTrue();
		}
	}

	private boolean runTest(String requiredTest, String latex) throws ContradictionException, TimeoutException {
		String nameOfSystem = getNameOfSystem(requiredTest);
		String sequent = getSequent(requiredTest);
		String isDerivable = getIsDerivable(requiredTest);

		QueryFormData q = new QueryFormData(QueryFormData.getFormDataOfCalculus(nameOfSystem)); //copy constructor
		q.sequent = sequent;
		if (latex.contains("true")) {
			q.texify();
		}
		boolean shouldBeDerivable = isDerivable.contains("true") ? true : false;
		return runFromQueryFormData(q, shouldBeDerivable);
	}

	public boolean runFromQueryFormData(QueryFormData q, boolean shouldBeDerivable) throws ContradictionException, TimeoutException {
		Language l = Language.makeInstance(q.propositionalVariables, q.propositionalConnectives, q.nextOperators);
		SequentCalculus G = SequentCalculus.makeInstance(q.getRules(), l, q.analyticityWrtTo);
		Sequent s = Sequent.makeInstance(q.sequent, l);
		DecisionProcedure d = new DecisionProcedure(G, new DecisionProperties(10, false));
		DecisionResult decisionResult = d.decide(s);
		boolean result = (shouldBeDerivable == decisionResult.isProvable());
		return result;

	}

	private String getNameOfSystem(String requiredTest) {
		List<String> details = StringUtils.splitAndTrimToList(requiredTest, "\n");
		String result = StringUtils.splitAndTrimToList(details.get(0), ":").get(1);
		return result;
	}

	private String getSequent(String requiredTest) {
		List<String> details = StringUtils.splitAndTrimToList(requiredTest, "\n");
		String sequentPart = details.get(1);
		List<String> splitedSequentPart = StringUtils.splitAndTrimBalancedToList(sequentPart, ":");
		String result = splitedSequentPart.get(1);
		return result;
	}

	private String getIsDerivable(String requiredTest) {
		List<String> details = StringUtils.splitAndTrimToList(requiredTest, "\n");
		String result = StringUtils.splitAndTrimToList(details.get(2), ":").get(1);
		return result;
	}


	private Set<Formula> createFormulas(List<String> formulasStrings, Language L) {
		Set<Formula> result = new LinkedHashSet<Formula>();
		for (String f : formulasStrings) {
			result.add(Formula.makeInstance(f, L));
		}
		return result;
	}


}
