package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import models.DecisionProcedure;
import models.DecisionProperties;
import models.DecisionResult;
import models.Language;
import models.Sequent;
import models.SequentCalculus;

import org.sat4j.specs.ContradictionException;

import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.StringUtils;
import views.formdata.QueryFormData;
import views.html.Index;



/**
 * The controller for the single page of this application.
 *
 * @author Philip Johnson
 */
public class Application extends Controller {
	
	
	/**
	 * Returns the page where the form is filled by the Student whose id is
	 * passed, or an empty form if the id is 0.
	 * 
	 * @param id
	 *            The id of the Student whose data is to be shown. 0 if an empty
	 *            form is to be shown.
	 * @return The page containing the form and data.
	 * @throws IOException 
	 */
	public static Result getIndex(String id, String latex) throws IOException {
		QueryFormData.addPredefinedSystems();
		QueryFormData queryData = new QueryFormData();
		Form<QueryFormData> formData;		
		queryData = QueryFormData.getFormDataOfCalculus(id);
		if (latex.contains("true")) {
			queryData.texify();
		}
		formData = Form.form(QueryFormData.class).fill(
				queryData);
		return ok(Index.render(formData,
				"",
				"",
				QueryFormData.getPredefinedCalculi(),
				id, 
				QueryFormData.getParalyzerOptions(), 
				getNumOfLines(id),
				QueryFormData.getMapForLatex(latex)));
	}

	/**
	 * @return The index page with the results of validation.
	 * @throws ContradictionException 
	 * @throws IOException 
	 */
	public static Result postIndex(String id, String latex) throws IOException {
		QueryFormData.addPredefinedSystems();
		long startTime = System.currentTimeMillis();
		// Get the submitted form data from the request object, and run
		// validation
		Form<QueryFormData> formData = Form.form(QueryFormData.class).bindFromRequest();
		if (formData.hasErrors()) {
			// Don't call formData.get() when there are errors, pass 'null' to
			// helpers instead.
			flash("error", "Please correct errors above.");
			return badRequest(Index.render(formData, "", "", QueryFormData.getPredefinedCalculi(), id,    QueryFormData.getParalyzerOptions(), getNumOfLines(id),QueryFormData.getMapForLatex(latex)));
		} else {
			List<String> errors = new ArrayList<String>();  //these are errors that we find during parsing
			QueryFormData  queryFormData = formData.get();
			if (id.equals("paralyzer")) { 
				queryFormData = QueryFormData.createFromParalyzer(queryFormData.getChosenParalyzerOption(), 
						queryFormData.fromParalyzer, queryFormData.sequent, queryFormData.timeout);
			}
			Language L = Language.makeInstance(queryFormData.propositionalVariables, 
					queryFormData.propositionalConnectives, 
					queryFormData.nextOperators);
			SequentCalculus G = SequentCalculus.makeInstance(queryFormData.rules, L, queryFormData.analyticityWrtTo);
			if (G.hasLonelyAtoms()) {
				errors.add("The calculus is not standard, in the sense that it has a rule in which one of the propositional variables is  not a subformula of any other formula in the rule.");
			}

			Sequent s = Sequent.makeInstance(queryFormData.sequent, L);
			int timeout = Integer.parseInt(queryFormData.timeout);
			flash("success", "");
			DecisionProperties decisionProperties = new DecisionProperties(timeout, true);
			DecisionProcedure decisionProcedure = new DecisionProcedure(G, decisionProperties);
			String result;
			DecisionResult decisionResult;
			decisionResult = decisionProcedure.decide(s);
			result = decisionResult.isProvable() ? "provable": "unprovable";
			String bottomLine = "";
			String details = "";

			long endTime = System.currentTimeMillis();
			if (errors.isEmpty()) {				
				bottomLine = getBottomLine(result);
				details = PrettyDetails.getDetails(L, G, s, decisionResult, endTime - startTime);
			} else {
				bottomLine = 
						"<p style=\"color:black;font-weight:bold;text-align:center\">There are some problems with your input. Details below.</p>";
				details = getErrors(errors);
			}
			return ok(Index.render(formData, details, bottomLine, QueryFormData.getPredefinedCalculi(), id, QueryFormData.getParalyzerOptions(), getNumOfLines(id),queryFormData.getMapForLatex()));
		}
		
		
	}

	private static String getErrors(List<String> errors) {
		String result = "<h4>Problems:</h4><ol>";
		for (String error : errors) {
			result += "<li>" + error + "</li>";
		}
		result+="</ol>";
		return result;
	}

	private static String getNumOfLines(String id) {
		int numOfLines = StringUtils.countLines(QueryFormData.getFormDataOfCalculus(id).getRules()) +1;
		String numOfLinesString = Integer.toString(numOfLines);
		return numOfLinesString;
	}


	private static String getBottomLine(String result) {
		String provable = "<p style=\"color:green;font-weight:bold;text-align:center\">The Sequent is Provable!</p>";
		String unprovable = "<p style=\"color:red; font-weight:bold;text-align:center\">The Sequent is not Provable!</p>";
		String timeout = "<p style=\"color:blue; font-weight:bold;text-align:center\">The Sat-Solver Reached its Time Limit </p>";

		if (result.equals("timeout")) {
			return timeout;
		} else if (result.equals("provable")) {
			return provable;
		} else {
			return unprovable;
		}
	}

}
