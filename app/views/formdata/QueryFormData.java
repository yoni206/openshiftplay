package views.formdata;

import java.io.IOException; 
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import play.Play;
import play.data.validation.ValidationError;
import utils.FileUtilsInternal;
import utils.MultiMap;
import utils.StringUtils;

public class QueryFormData {

	public String propositionalVariables = "";
	public String propositionalConnectives = "";
	public String nextOperators = "";
	public String rules = "";
	public List<String> latex = new ArrayList<String>() ;

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public String analyticityWrtTo = "";
	public String sequent = "";
	public String timeout = "10";
	public String chosenParalyzerOption = "";
	public String fromParalyzer = "";

	public String getFromParalyzer() {
		return fromParalyzer;
	}

	public void setFromParalyzer(String fromParalyzer) {
		this.fromParalyzer = fromParalyzer;
	}

	public String getChosenParalyzerOption() {
		return chosenParalyzerOption;
	}

	public void setChosenParalyzerOption(String chosenParalyzerOption) {
		this.chosenParalyzerOption = chosenParalyzerOption;
	}

	private static Map<String, QueryFormData> famousCalculiFormDatas = new HashMap<String, QueryFormData>();

	public static Map<String, QueryFormData> getFamousCalculiFormDatas() {
		return famousCalculiFormDatas;
	}

	public static void setFamousCalculiFormDatas(
			Map<String, QueryFormData> famousCalculiFormDatas) {
		QueryFormData.famousCalculiFormDatas = famousCalculiFormDatas;
	}

	public static List<String> calculiNames = new ArrayList<String>();

	public static void addPredefinedSystems() throws IOException {
		InputStream is = Play.application().classloader().getResourceAsStream("predefined.txt");
		String preDefinedSystemsString = FileUtilsInternal.fromStream(is);
		preDefinedSystemsString = preDefinedSystemsString.replace("\r\n", "\n");
		List<String> stringsOfSystems = StringUtils.splitAndTrimToList(preDefinedSystemsString, "\n\n"); 
		for (String stringOfSystem : stringsOfSystems) {
			addSystem(stringOfSystem);
		}
	}
	
	public static QueryFormData makeInstance(String stringOfSystem) {
		MultiMap<String, String> lines = StringUtils.getLinesAsMultiMap(stringOfSystem, ":");
		return makeInstance(lines);
		
	}
	
	public static QueryFormData makeInstance(MultiMap<String, String> lines) {
		String displayName="", connectives="", nextOperators="", rules="", analyticity="", inputSequent = "";
		displayName = lines.containsKey("displayName") ? lines.getSome("displayName") : "";
		connectives = lines.containsKey("connectives") ? lines.getSome("connectives") : "";
		nextOperators = lines.containsKey("nextOperators")?  lines.getSome("nextOperators") : "";
		analyticity = lines.containsKey("analyticity") ?  lines.getSome("analyticity") : "";
		inputSequent = lines.containsKey("inputSequent") ?  lines.getSome("inputSequent") : "";
		if (lines.containsKey("rule")) {
			for (String rule : lines.getAll("rule")) {
				rules += "\n" + rule;
			}
		}
		QueryFormData formData = new QueryFormData(getPropositionalAtoms(),
				connectives, nextOperators, rules, analyticity,
				inputSequent);
		return formData;
	}

	private static String getName(String stringOfSystem) {
		MultiMap<String, String> lines = StringUtils.getLinesAsMultiMap(stringOfSystem, ":");
		String name = lines.getSome("name");
		if (name == null) {
			name = "";
		}
		return name;
	}

	//The returned value is optional. It is used only in Application.main
	public static void addSystem(String stringOfSystem) {
		String name = getName(stringOfSystem);
		QueryFormData formData = makeInstance(stringOfSystem);
		if (!calculiNames.contains(name)) {			
			calculiNames.add(name);
			famousCalculiFormDatas.put(name, formData);
		}
	}


	private static String texify(String str) {
		return StringUtils.texify(str);
	}


	private static String getPropositionalAtoms() {
		return "p1,p2,p3,p4,p5,p6";
	}


	private static String getPositiveLK() {
		return "=>p1;=>p2/=>p1 AND p2" + "\n" + "p1,p2=>/p1 AND p2=>" + "\n"
				+ "p1=>;p2=>/p1 OR p2=>" + "\n" + "=>p1,p2 / => p1 OR p2"
				+ "\n" + "p1=>p2/=>p1 IMPLIES p2" + "\n"
				+ "=>p1; p2=> / p1 IMPLIES p2 =>" + "\n";
	}

	private static String getBK() {
		return getPositiveLK() + "p1=>/*1p1" + "\n" + "=>p1; =>*1p1/*2p1=>"
				+ "\n" + "p1,*1p1=>/=>*2p1" + "\n";
	}

	/** Required for form instantiation. */
	public QueryFormData() {

	}

	public QueryFormData(String propositionalVariables,
			String propositionalConnectives, String nextOperators,
			String rules, String analyticityWrtTo, String sequent,
			String chosenParalyzerOption, String fromParalyzer) {
		this.propositionalVariables = propositionalVariables;
		this.propositionalConnectives = propositionalConnectives;
		this.nextOperators = nextOperators;
		this.rules = rules;
		this.analyticityWrtTo = analyticityWrtTo;
		this.sequent = sequent;
		this.chosenParalyzerOption = chosenParalyzerOption;
		this.fromParalyzer = fromParalyzer;
	}

	public QueryFormData(String propositionalVariables,
			String propositionalConnectives, String nextOperators,
			String rules, String analyticityWrtTo, String sequent) {
		this(propositionalVariables, propositionalConnectives, nextOperators,
				rules, analyticityWrtTo, sequent, "", "");
	}
	
	public QueryFormData(QueryFormData q) {
		this(q.propositionalVariables,q.propositionalConnectives,q.nextOperators,q.rules,q.analyticityWrtTo,q.sequent,q.chosenParalyzerOption,q.fromParalyzer);
	}

	public List<ValidationError> validate() {
		List<ValidationError> result = new ArrayList<ValidationError>();
		if (this.sequent.isEmpty()) {
			result.add(new ValidationError("sequent", "Please enter a sequent."));
		}

		if ((!this.sequent.contains("=>"))
				&& (!this.sequent.contains("\\Rightarrow"))) {
			result.add(new ValidationError("sequent",
					"Sequent must include '=>'."));
		} else {
			if (!StringUtils.balanced(this.sequent)) {
				result.add(new ValidationError("sequent",
						"Some of the formulas are missing parenthesis."));
			}
		}

		Set<String> connectives = StringUtils.split(
				this.propositionalConnectives, ",");
		for (String c : connectives) {
			if (!c.contains(":")) {
				result.add(new ValidationError("propositionalConnectives",
						"Please specify the arity of each connective (e.g. AND:2,OR:2)"));
			}
		}
		
		if (!this.fromParalyzer.isEmpty() && this.fromParalyzer.contains("{}")) {
			result.add(new ValidationError(
					"fromParalyzer",
					"The calculus is not analytic, as it includes a truth table with an empty ({}) line"));
		}
		

		if (result.isEmpty()) {
			result = null;
		}
		return result;

	}

	public static QueryFormData getFormDataOfCalculus(String name) {
		QueryFormData result = new QueryFormData();
		if (name.equals("paralyzer")) {
			result.setChosenParalyzerOption("LK+");
		} else {
			result = famousCalculiFormDatas.containsKey(name) ? famousCalculiFormDatas.get(name) : result;
		}
		return new QueryFormData(result);
		// we return a new clone because we do not want changes to affect famousCalculiFormDatas.
	}
	

	
	
	public static List<String> getPredefinedCalculi() {
		return calculiNames;
	}

	public static List<String> getParalyzerOptions() {
		List<String> result = new ArrayList<String>();
		result.add("LK+");
		result.add("BK");
		return result;
	}

	@Override
	public String toString() {
		return "QueryFormData [propositionalVariables="
				+ propositionalVariables + ", propositionalConnectives="
				+ propositionalConnectives + ", nextOperators=" + nextOperators
				+ ", rules=" + rules + ", analyticityWrtTo=" + analyticityWrtTo
				+ ", sequent=" + sequent + ", timeout=" + timeout
				+ ", chosenParalyzerOption=" + chosenParalyzerOption + "]";
	}

	public static QueryFormData createFromParalyzer(String startWith,
			String paralyzerOutput, String sequent, String timeout) {
		QueryFormData result = new QueryFormData();
		result.propositionalVariables = getPropositionalVariablesFromParalyzer(paralyzerOutput);
		result.propositionalConnectives = getPropositionalConnectivesFromParalyzer(paralyzerOutput);
		result.chosenParalyzerOption = startWith;
		result.analyticityWrtTo = getAnalyticityFromParalyzer(paralyzerOutput);
		result.nextOperators = "";
		result.rules = getRulesFromParalizer(paralyzerOutput, startWith);
		result.sequent = sequent;
		result.timeout = timeout;
		// if the calculus is not analytic, let the user know
		if (result.analyticityWrtTo == null) {
			result = null;
		}
		return result;
	}

	private static String getRulesFromParalizer(String paralyzerOutput,
			String startWith) {
		String result = getParalyzerRulesPart(paralyzerOutput);
		result = result.replaceAll(",'D'.*'G'", ";");
		result = result.replaceAll("'D'.*'G'", ";");
		result = result.replaceAll("\\r\\n[-]+\\r\\n", "/");
		result = result.replaceAll("\\n[-]+\\n", "/");
		result = result.replaceAll("'G',", "");
		result = result.replaceAll(",'D'", "");
		result = result.replaceAll("G", "");
		result = result.replaceAll("D", "");
		result = result.replaceAll("'", "");
		return startWith.equals("LK+") ? paralyze(getPositiveLK()) + result
				: paralyze(getBK()) + result;
	}

	private static String paralyze(String str) {
		return str.replaceAll("AND", "&").replaceAll("OR", "v")
				.replaceAll("IMPLIES", "->");
	}

	private static String getParalyzerRulesPart(String paralyzerOutput) {
		int begin = paralyzerOutput.indexOf("Equivalent Logical Rule(s):")
				+ "Equivalent Logical Rule(s):".length();
		int end = paralyzerOutput.indexOf("V_M:");
		return paralyzerOutput.substring(begin, end);
	}

	private static String getAnalyticityFromParalyzer(String paralyzerOutput) {
		if (paralyzerOutput.contains("{}")) {
			return null;
		} else {
			return "*1,*2";
		}
	}

	private static String getPropositionalConnectivesFromParalyzer(
			String paralyzerOutput) {
		String result = "&:2, v:2, ->:2, *1:1, *2:1";
		return result;
	}

	private static String getPropositionalVariablesFromParalyzer(
			String paralyzerOutput) {
		return "";
	}

	public static void main(String[] args) {
		
	}

//	public void normalizeTexInput() {
//		this.analyticityWrtTo = StringUtils.putDoubleSlashesAndDoubleArrow(this.analyticityWrtTo);
//		this.nextOperators = StringUtils.putDoubleSlashesAndDoubleArrow(this.nextOperators);
//		this.propositionalConnectives = StringUtils.putDoubleSlashesAndDoubleArrow(this.propositionalConnectives);
//		this.propositionalVariables = StringUtils.putDoubleSlashesAndDoubleArrow(this.propositionalVariables);
//		this.rules = StringUtils.putDoubleSlashesAndDoubleArrow(this.rules);
//		this.sequent = StringUtils.putDoubleSlashesAndDoubleArrow(this.sequent);
//	}


	public static Map<String, Boolean> getMapForLatex(String latex) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		if (latex.equals("true")) {
			result.put("latex", Boolean.TRUE);
		} else {
			result.put("latex", Boolean.FALSE);
		}
		return result;
	}

	public Map<String, Boolean> getMapForLatex() {
		return getMapForLatex(latex.isEmpty() ? "false" : "true");
	}

	public void texify() {
		this.propositionalVariables = texify(this.propositionalVariables);
		this.propositionalConnectives = texify(this.propositionalConnectives);
		this.nextOperators = texify(this.nextOperators);
		this.rules = texify(this.rules);
		this.analyticityWrtTo = texify(this.analyticityWrtTo);
		this.sequent = texify(this.sequent);
	}

}
